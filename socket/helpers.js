import * as config from "./config";

export const filterRooms = rooms => {
    const limit = config.MAXIMUM_USERS_FOR_ONE_ROOM;
    let newRooms = Array.from(rooms.values());
    const result = newRooms.filter(room => room.users.length !== room.usersReady && room.users.length < limit);

    return result;
}
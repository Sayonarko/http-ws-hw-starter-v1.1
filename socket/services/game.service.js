import * as config from "../config";
import { texts } from "../../data";
import { filterRooms } from "../helpers";
import Commentator from "./commentator.service.js";

export default class Game {
    constructor(rooms, io, socket) {
        this.rooms = rooms;
        this.io = io;
        this.socket = socket;
        this.commentatorService = new Commentator(rooms, io, socket);
    }

    get username() {
        return this.socket.handshake.query.username;
    }

    readyGame({ roomId }) {
        const room = this.rooms.get(roomId);

        room?.users.forEach(user => {

            if (user.username === this.username) {
                user.ready = !user.ready;

                user.ready
                    ? room.usersReady++
                    : room.usersReady--;

                this.commentatorService.readyUser(user, roomId);
            }
        });

        if (room?.users.length === room.usersReady) {
            let timer = config.SECONDS_TIMER_BEFORE_START_GAME;

            this.commentatorService.startTimer(roomId);

            const interval = setInterval(() => {
                timer--;
                this.io.to(roomId).emit("START_TIMER", timer);

                if (timer === 0) {
                    clearInterval(interval);

                    const payload = {
                        roomId,
                        counter: config.SECONDS_FOR_GAME,
                        id: Math.floor(Math.random() * texts.length)
                    };

                    this.io.to(roomId).emit("START_GAME", payload);

                    this.commentatorService.startGame(roomId);

                    let gameCounter = config.SECONDS_FOR_GAME;

                    const counterInterval = setInterval(() => {
                        const room = this.rooms.get(roomId);

                        this.commentatorService.checkPoint(gameCounter, room, roomId);

                        if (room?.users.length === room?.winners.length) {
                            clearInterval(counterInterval);
                        }

                        if (gameCounter % 21 === 0) {
                            this.commentatorService.randomFact(roomId);
                        }

                        if (gameCounter === 0) {
                            clearInterval(counterInterval);

                            let winners = room?.users.sort((a, b) => b.progress - a.progress);
                            const getUsername = user => user.username;

                            winners = winners.map(getUsername);
                            room.winners = winners;

                            this.io.to(roomId).emit("END_GAME", roomId);
                            this.commentatorService.endGame(winners, roomId);
                        }

                        gameCounter--;
                        this.io.to(roomId).emit("UPDATE_COUNTER", gameCounter);
                    }, 1000);
                }
            }, 1000);
        }

        this.rooms.set(roomId, room);
        this.io.to(roomId).emit("UPDATE_ROOM", room);
        this.socket.broadcast.emit("UPDATE_ROOMS", filterRooms(this.rooms));
    };


    updateProgress({ progress, roomId }) {
        this.io.to(roomId).emit("UPDATE_PROGRESS", { username: this.username, progress });
        const room = this.rooms.get(roomId);

        room?.users.forEach(user => {
            if (user.username === this.username) {
                user.progress = progress;
            }
        });

        this.rooms.set(roomId, room);

        if (progress > 75 && progress < 85) {
            this.commentatorService.playOff(this.username, roomId);
        }

        if (progress === 100) {
            room.winners = [...room.winners, this.username]
            this.rooms.set(roomId, room);
            this.commentatorService.finishedUser(this.username, roomId);

            if (room?.winners.length === room?.users.length) {
                this.io.to(roomId).emit("END_GAME", roomId);
                this.commentatorService.endGame(room.winners, roomId);
            }
        }
    }

    newGame(roomId) {
        const room = this.rooms.get(roomId);

        room?.users.forEach(user => {
            user.progress = 0;
            user.ready = false;
        })

        room.usersReady = 0;
        room.winners = [];
        this.rooms.set(roomId, room);
        this.io.to(roomId).emit("UPDATE_ROOM", room);
        this.socket.broadcast.emit("UPDATE_ROOMS", filterRooms(this.rooms));
        this.commentatorService.newGame(room.users, roomId);
    }
}
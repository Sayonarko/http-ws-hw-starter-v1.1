import { messages } from "../../data.js";
import * as config from "../config";

export default class Commentator {
    constructor(rooms, io, socket) {
        this.rooms = rooms;
        this.io = io;
        this.socket = socket;
    }

    get username() {
        return this.socket.handshake.query.username;
    }

    greetingUser(room, roomId) {

        room.users.length === 1
            ? this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.greetingFirstUser(this.username))
            : this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.greetingUser(this.username));
    }

    leaveUser(roomId) {
        this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.leaveUser(this.username));
    }

    readyUser(user, roomId) {
        user.ready
            ? this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.confirmReady(this.username))
            : this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.unconfirmReady(this.username));
    }

    startTimer(roomId) {
        this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.startTimer(this.username));
    }

    startGame(roomId) {
        this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.startGame());
    }

    checkPoint(gameCounter, room, roomId) {
        const timer = config.SECONDS_FOR_GAME;
        const users = room?.users.sort((a, b) => b.progress - a.progress);

        if (room?.winners.length !== room?.users.length
            && gameCounter !== timer
            && gameCounter !== 0
            && (timer - gameCounter) % 30 === 0) {
            const time = timer - gameCounter;

            this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.everyHalfMinute(users, time, gameCounter));
        }
    }

    playOff(username, roomId) {
        this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.playOff(username));
    }

    finishedUser(username, roomId) {
        this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.finishedUser(username));
    }

    endGame(users, roomId) {
        this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.endGame(users));
    }

    newGame(users, roomId) {
        this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.newGame(users));
    }

    randomFact(roomId) {
        this.io.to(roomId).emit("UPDATE_COMMENTATOR", messages.randomFact());
    }
}
import { filterRooms } from "../helpers.js";
import Commentator from "./commentator.service.js";

export default class Room {
    constructor(rooms, io, socket) {
        this.rooms = rooms;
        this.io = io;
        this.socket = socket;
        this.commentatorService = new Commentator(rooms, io, socket);
        this.joinRoom = this.joinRoom.bind(this);
        this.leaveRoom = this.leaveRoom.bind(this);
    }

    get username() {
        return this.socket.handshake.query.username;
    }

    createRoom(name) {

        if (this.rooms.has(name)) {
            this.socket.emit("ROOM_EXIST");
        } else {
            this.rooms.set(name, { name, users: [], usersReady: 0, winners: [] });
            this.joinRoom(name);
        }
    };

    joinRoom(roomId) {
        this.socket.join(roomId, () => {
            const room = this.rooms.get(roomId);
            const user = {
                username: this.username,
                progress: 0,
                ready: false
            };

            room.users = [...room.users, user]
            this.rooms.set(roomId, room);

            this.commentatorService.greetingUser(room, roomId);

            this.io.to(this.socket.id).emit("JOIN_ROOM_DONE", room);
            this.io.to(roomId).emit("UPDATE_ROOM", room);
            this.socket.broadcast.emit("UPDATE_ROOMS", filterRooms(this.rooms));
        });
    };

    leaveRoom(roomId) {

        this.socket.leave(roomId, () => {
            const room = this.rooms.get(roomId);

            if (room?.users.length > 1) {
                const updateUsers = room.users.filter(user => user.username !== this.username);
                room.users = updateUsers;
                this.rooms.set(roomId, room);

                this.io.to(this.socket.id).emit("LEAVE_ROOM_DONE", filterRooms(this.rooms));
                this.io.to(roomId).emit("UPDATE_ROOM", room);
                this.commentatorService.leaveUser(roomId);
            } else {
                this.rooms.delete(roomId);
                this.io.to(this.socket.id).emit("LEAVE_ROOM_DONE", filterRooms(this.rooms));
                this.socket.broadcast.emit("UPDATE_ROOMS", filterRooms(this.rooms));
            }
        });
    }

    leaveAllRooms(rooms) {
        Object.keys(rooms).forEach(room => {
            this.leaveRoom(room);
        })
    }
}
import Room from "./room.service.js";
import Game from "./game.service.js";
import Commentator from "./commentator.service.js";

const username = sessionStorage.getItem("username");
const socket = io("", { query: { username } });
const roomsPage = document.getElementById("rooms-page");
const roomsContainer = document.getElementById("rooms-container");
const gamePage = document.getElementById("game-page");
const createRoomBtn = document.getElementById("add-room-btn");

if (!username) {
  window.location.replace("/login");
}

const roomService = new Room({
  io,
  socket,
  roomsPage,
  roomsContainer,
  gamePage
});
const gameService = new Game({ socket });
const commentatorService = new Commentator({ socket });

createRoomBtn.addEventListener("click", () => roomService.createRoom());

socket.on("USER_EXIST", () => roomService.userExist());
socket.on("UPDATE_ROOMS", rooms => roomService.updateRooms(rooms));
socket.on("UPDATE_ROOM", room => roomService.joinRoomDone(room));
socket.on("JOIN_ROOM_DONE", room => roomService.joinRoomDone(room));
socket.on("ROOM_EXIST", () => window.alert("This room name already exists."));
socket.on("LEAVE_ROOM_DONE", rooms => roomService.leaveRoomDone(rooms));
socket.on("START_TIMER", timer => gameService.startTimer(timer));
socket.on("START_GAME", payload => gameService.startGame(payload));
socket.on("UPDATE_PROGRESS", payload => gameService.updateProgress(payload));
socket.on("UPDATE_COUNTER", counter => gameService.updateCounter(counter));
socket.on("END_GAME", roomId => gameService.endGame(roomId));
socket.on("UPDATE_COMMENTATOR", message => commentatorService.updateCommentator(message));
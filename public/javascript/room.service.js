import { createElement, addClass, removeClass } from "./helpers.mjs";
import commentatorImage from "../ico/commentator.js";

class Room {
    constructor({ io, socket, roomsPage, roomsContainer, gamePage }) {
        this.io = io;
        this.socket = socket;
        this.addRoom = this.addRoom.bind(this);
        this.updateRooms = this.updateRooms.bind(this);
        this.roomsPage = roomsPage;
        this.roomsContainer = roomsContainer;
        this.gamePage = gamePage;
    }

    get username() {
        return sessionStorage.getItem("username");
    }

    userExist() {
        window.alert("User already exists! Please enter another username.");
        sessionStorage.removeItem("username");
        window.location.replace("/login");
    }

    createRoom() {
        const name = window.prompt("Please enter the name of the room:");
        if (name) this.socket.emit("CREATE_ROOM", name);
    }

    addRoom(room) {
        const roomElement = createElement({
            tagName: "div",
            className: "room",
            attributes: { id: room.name }
        });

        const roomUsers = createElement({
            tagName: "span"
        });

        const roomTitle = createElement({
            tagName: "h1"
        });

        const roomButton = createElement({
            tagName: "button",
            className: "join-btn"
        })
        roomTitle.innerText = room.name;
        roomUsers.innerText = `${room.users.length} ${room.users.length === 1 ? 'user' : 'users'} connected`;
        roomButton.innerText = "Join";

        const onJoinRoom = () => {
            this.socket.emit("JOIN_ROOM", room.name);
        };

        roomButton.addEventListener("click", onJoinRoom);
        roomElement.append(roomUsers, roomTitle, roomButton);

        return roomElement;
    }

    updateRooms(rooms) {
        const allRooms = rooms.map(this.addRoom);
        this.roomsContainer.innerHTML = "";
        this.roomsContainer.append(...allRooms);
    };

    joinRoomDone(room) {
        addClass(this.roomsPage, "display-none");
        removeClass(this.gamePage, "display-none");

        const users = room.users.map(user => {
            return (
                `<div class="user">
                <div class="user-name">
                  <span class="ready-status-${user.ready ? 'green' : 'red'}"></span>
                  ${user.username} ${user.username === this.username ? ' (you)' : ''}
                </div>
                <div class="user-progress ${user.username}">
                  <div id="progress-bar-${user.username}" class="progress-bar"></div>
              </div>
              </div>`
            );
        });

        const game = createElement({
            tagName: "div"
        });

        game.innerHTML = `
                <div class="container container-grid">
                  <div>
                    <h1>${room.name}</h1>
                    <button id="quit-room-btn" class="large-btn">Back To Rooms</button>
                    ${users.join('')}
                  </div>
                  <div class="text-container" id="text-container">
                      <button id="ready-btn" class="ready-btn">READY</button>
                  </div>
                  <div class="commentator">
                    <div class="commentator-text-container" id="commentator-text-container">
                    </div>
                  <div class="commentator-avatar">
                    ${commentatorImage}
                    </div>
                  </div>
                </div>
                `;

        const onLeaveRoom = () => {
            this.socket.emit("LEAVE_ROOM", room.name);
            if (room.users.length > 1) {
                this.socket.emit("READY_GAME", { roomId: room.name });
                this.socket.emit('UPDATE_PROGRESS', { progress, roomId });
            }
        }

        const onReady = () => {
            this.socket.emit("READY_GAME", { roomId: room.name });
        }

        this.gamePage.innerHTML = '';
        this.gamePage.appendChild(game);

        const quitBtn = document.getElementById("quit-room-btn");
        const readyBtn = document.getElementById("ready-btn");

        readyBtn.addEventListener("click", onReady);
        quitBtn.addEventListener("click", onLeaveRoom);

        room.users.forEach(user => {
            if (user.username === this.username) {
                readyBtn.innerHTML = user.ready ? "NOT READY" : "READY";
            }
        })
    }

    leaveRoomDone(rooms) {
        addClass(this.gamePage, "display-none");
        removeClass(this.roomsPage, "display-none");
        this.updateRooms(rooms);
    }
}

export default Room;
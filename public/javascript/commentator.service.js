import { createElement } from "./helpers.mjs";

class Commentator {
    constructor({ socket }) {
        this.socket = socket;
    }

    get textContainer() {
        return document.getElementById("commentator-text-container");
    }

    updateCommentator(message) {
        const newMessage = createElement({
            tagName: "div",
            className: "commentator-text"
        });
        newMessage.innerText = message;
        setTimeout(() => {
            this.textContainer.innerHTML = '';
            this.textContainer.append(newMessage);
        }, 200);
    }
}

export default Commentator;
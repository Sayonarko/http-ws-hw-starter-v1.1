import { createElement, addClass, removeClass } from "./helpers.mjs";

class Game {
    constructor({ socket }) {
        this.socket = socket;
    }

    get username() {
        return sessionStorage.getItem("username");
    }

    get textContainer() {
        return document.getElementById("text-container");
    }

    startTimer(timer) {
        const quitBtn = document.getElementById("quit-room-btn");

        addClass(quitBtn, "visibility-none");
        this.textContainer.innerText = timer;
    }

    startGame({ counter, id, roomId }) {

        const textElement = createElement({
            tagName: "div",
            className: "text-game"
        });

        const countElement = createElement({
            tagName: "span",
            className: "game-counter",
            attributes: { id: "game-counter" }
        });

        countElement.innerText = `${counter} seconds left`;

        fetch('http://localhost:3002/game/texts/' + id)
            .then(res => res.json())
            .then(result => {
                const letters = result.split("").map(letter => `<span class="letter">${letter}</span>`);

                textElement.innerHTML = letters.join('');
                this.textContainer.innerHTML = '';
                this.textContainer.appendChild(countElement);
                this.textContainer.appendChild(textElement);

                const letterElements = document.querySelectorAll('.letter');
                addClass(letterElements[0], "letter-next");

                let index = 0;
                const persent = 100 / result.length;
                const onKeyUp = e => {

                    if (e.key === result[index]) {
                        removeClass(letterElements[index], "letter-next");
                        addClass(letterElements[index], "letter-done");
                        if (letterElements[index + 1]) addClass(letterElements[index + 1], "letter-next");
                        index++;
                        let progress = persent * index;
                        this.socket.emit('UPDATE_PROGRESS', { progress, roomId })
                    }
                }
                window.addEventListener("keyup", onKeyUp);
            })
    }

    updateProgress({ username, progress }) {
        const progressBar = document.getElementById(`progress-bar-${username}`);
        progressBar.style.width = progress + '%';

        if (progress === 100) {
            progressBar.style.width = progress + '%';
            progressBar.style.backgroundColor = 'green';
        }
    }

    updateCounter(counter) {
        const countElement = document.getElementById("game-counter");
        if (countElement) countElement.innerText = `${counter} seconds left`;
    }

    endGame(roomId) {
        const newGameBtn = createElement({
            tagName: "button",
            className: "new-game-btn",
            id: "quit-results-btn"
        });
        newGameBtn.innerText = "New Game";
        this.textContainer.innerHTML = "";
        this.textContainer.appendChild(newGameBtn);
        newGameBtn.addEventListener("click", () => this.socket.emit("NEW_GAME", roomId));
    }

}

export default Game;
export const texts = [
  "Text for typing #1",
  "Text for typing #2",
  "Text for typing #3",
  "Text for typing #4",
  "Text for typing #5",
  "Text for typing #6",
  "Text for typing #7",
];

export const messages = {
  greetingFirstUser: function (username) {
    return `Приветствуем первого участника гонки - ${username}!\n\nПервый на старте - первый на финише!`
  },
  greetingUser: function (username) {
    let message = `К нашей гонке присоединился новый участник. Поприветствуем ${username}!`;
    const desc = [
      "\n Этот парень так быстр и опасен, что у Вин Дизеля волосы на голове шевелятся.",
      "\n Он самый быстрый гонщик на Диком Западе!",
      "\n Широко известный в узких кругах чемпион гонки 24 часа Ле-Ман 2007 года",
      "\n Обладателя мировых рекордов и девичьих сердец",
    ]
    const random = Math.floor(Math.random() * desc.length);
    return message += desc[random];
  },
  leaveUser: function (username) {
    return `К сожалению ${username} покинул нашу прекрасную гонку... Надеемся он скоро вернется!`
  },
  confirmReady: function (username) {
    return `${username} решительно настроен на победу и уже подтвердил свою готовность. Остальным участникам стоит поспешить!`;
  },
  unconfirmReady: function (username) {
    return `У ${username} технические неполадки с двигателем и ему потребуется пару лишних минут... Ну что же, подождем`
  },
  startTimer: function () {
    return `Последние секунды перед стартом! Желаем всем участникам удачи!`
  },
  startGame: function () {
    return `Гонка началась!`
  },
  everyHalfMinute: function (users, time, gameCounter) {
    let message = `Прошло ${time} секунд нашей гонки, `

    users.forEach((user, id) => {
      switch (id) {
        case 0: message += `в которой ${user.username} мчит как ветер`;
          break;
        case 1: message += `, за ним по пятам идет ${user.username}`;
          break;
        case 2: message += ` и замыкает тройку лидеров ${user.username}!`;
          break;
        default: return;
      }
    });
    message += `\nПоднажмите, осталось ${gameCounter} секунд.`;

    return message;
  },
  finishedUser: function (username) {
    return `Невероятный ${username} пересек финишную черту!`
  },

  playOff: function (username) {
    return `${username} выходит на финишную прямую!`
  },
  endGame: function (users) {
    let message = `ФИНИШ!\nЛучшиe в этой гонке:\n`;

    users.forEach((user, id) => {
      if (id < 3) message += `#${id + 1} ${user}\n`;
    })

    return message;
  },
  newGame: function (users) {
    let message = ''
    users.forEach(user => {
      message += user.username + "! ";
    })
    message += " Это было здорово, давайте сыграем еще разок!";
    return message;
  },
  randomFact: function () {
    let message = "Интерестный факт:\n"
    const facts = [
      "Первый автомобиль с бензиновым двигателем был сконструирован Карлом Бенцом и начал первый выпуск автомобилей в 1888 году",
      "Самым быстрым седаном является ВАЗ 2105 1992 года выпуска, который под управлением курганца Егора Синишина развил скорость в 418,3 км/ч",
      "F1 болид может разгоняться от 0 до 160 километров в час и замедлиться до 0 км/ч всего за четыре секунды.",
      "Самая длинная в мире автомобильная пробка, длина которой составляла около 200 километров, случилась в 1980-м году во Франции.",
      "Самый большой автомобиль в мире — белорусский карьерный грузовик БелАЗ 75710. Высота этой модели превышает восемь метров, а масса — восемьсот тонн.",
      "Первый в мире нарушитель скоростного режима был оштрафован за скорость 13 км/ч",
      "Большинство личных автомобилей 95% времени стоят, а не ездят"
    ];
    const random = Math.floor(Math.random() * facts.length);

    return message += facts[random];
  }
}

export default { texts, messages };
